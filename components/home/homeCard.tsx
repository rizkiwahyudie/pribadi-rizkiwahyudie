import { Box, Heading, Text, Img, Flex } from "@chakra-ui/react";
import { BaseIcon } from "../base/baseIcon";
import { UilArrowUpRight } from "@iconscout/react-unicons";
import Home from "../../styles/Home.module.css";

interface Props {
  title: string;
  desc: string;
  cover: any;
  tech: any;
  link: string;
}

export default function HomeCard({ title, desc, tech, cover, link }: Props) {
  return (
    <Box w="100%" rounded={"md"} overflow={"hidden"}>
      <Box h={{ base: "255px", md: "230px" }}>
        <Img
          src={cover.src}
          // outline="3px solid rgb(45, 55, 72, 0.2)"
          rounded={"xl"}
          objectFit="cover"
          h="full"
          w="full"
          alt={"Blog Image"}
        />
      </Box>
      <Box pt={2.5} pb={4}>
        <a href={link} target="_blank">
          <Flex
            cursor="pointer"
            className={Home.titlePortfolio}
            transition="0.2s"
            alignItems="center"
          >
            <Heading fontSize={"2xl"} noOfLines={1}>
              {title}
            </Heading>
            <Box
              className={Home.directPortfolio}
              mb="-15px"
              opacity="0"
              transition="0.2s"
            >
              <UilArrowUpRight />
            </Box>
          </Flex>
        </a>
        <Flex mt={1.5}>
          {tech.map((item: any, index: number) => (
            <Box mr={2} key={index}>
              <BaseIcon tech={item} />
            </Box>
          ))}
        </Flex>
        <Text mt={3} noOfLines={3}>
          {desc}
        </Text>
      </Box>
    </Box>
  );
}
