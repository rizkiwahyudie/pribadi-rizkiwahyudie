import bootstrap from "@/public/iconTechStack/bootstrap.svg";
import css from "@/public/iconTechStack/css.svg";
import cypress from "@/public/iconTechStack/cypress.svg";
import figma from "@/public/iconTechStack/figma.svg";
import firebase from "@/public/iconTechStack/firebase.svg";
import javascript from "@/public/iconTechStack/javascript.svg";
import jquery from "@/public/iconTechStack/jquery.svg";
import mysql from "@/public/iconTechStack/mysql.svg";
import php from "@/public/iconTechStack/php.svg";
import postman from "@/public/iconTechStack/postman.svg";
import react from "@/public/iconTechStack/react.svg";
import tailwind from "@/public/iconTechStack/tailwind.svg";
import typescript from "@/public/iconTechStack/typescript.svg";
import vuejs from "@/public/iconTechStack/vuejs.svg";
import wordpress from "@/public/iconTechStack/wordpress.svg";
import cephatApp from "@/public/coverPortofolio/CephatApp.webp";
import findCoach from "@/public/coverPortofolio/FindCoach.webp";
import nusantaraTrip from "@/public/coverPortofolio/NusantaraTrip.webp"
import hurikenBot from "@/public/coverPortofolio/huriken.webp";

interface Newest_Portfolio {
  title: string,
  desc: string,
  cover: any,
  tech: Array<any>,
  link: string
}

export const NEWEST_PORTFOLIO: Array<Newest_Portfolio> = [
  {
    title: "Cephat App",
    desc: "Cephat app is a website-based application for calculatingcalories and user nutrition and can plan healthy foods according to the user's nutritional needs",
    cover: cephatApp,
    tech: [vuejs, tailwind],
    link: "https://cephat-app.netlify.com"
  },
  {
    title: "Find Coach App",
    desc: "This project is the result of a project for a final assignment in a Vue js course in udemy with coach Maximilian. Find Coach App is a Web-based application can send messages to coaches to communicate, and users can register as coaches",
    cover: findCoach,
    tech: [vuejs, tailwind, firebase],
    link: "https://findcoach-projectvue.web.app/"
  },
  {
    title: "Nusantara Trip App",
    desc: "Nusantara Trip is a web-based application hotel bookings and a special feature, there is a payment card in the application for booking payments. There are 2 roles, namely user and admin",
    cover: nusantaraTrip,
    tech: [php, mysql, css, javascript, jquery],
    link: "https://nusantaratrip.app.rovingstore.com"
  },
  {
    title: "Huriken Bot Trading App",
    desc: "Huriken app is a project that was developed when I was working on the RnD project. My task is to implement User Interface design into coding. Huriken app is a web-based application for trading app using bot",
    cover: hurikenBot,
    tech: [bootstrap, javascript, jquery],
    link: "https://channeling-huriken.netlify.app/"
  },
];
