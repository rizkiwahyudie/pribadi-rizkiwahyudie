import {
  Box,
  chakra,
  useColorModeValue,
  Flex,
  Heading,
  Text,
  Grid,
  GridItem,
} from "@chakra-ui/react";
import Image from "next/image";
import imgProf from "@/public/imgProfile.webp";
import {
  UilAt,
  UilLinkedin,
  UilGithub,
  UilInstagram,
} from "@iconscout/react-unicons";
import ColorMode from "../base/colorMode";
import Typed from "typed.js";
import { useEffect, useRef } from "react";
import TitleSection from "../base/titleSection";
import HomeCard from "./homeCard";
import { NEWEST_PORTFOLIO } from "./homePortolio";

export default function Homes() {
  const ImgProfile = chakra(Image);
  const desc = useRef(null);
  useEffect(() => {
    const typed = new Typed(desc.current, {
      strings: ["Student and Frontend Developer"],
      loop: true,
      typeSpeed: 35,
      backSpeed: 15,
      backDelay: 5000,
      showCursor: false,
    });
    return () => {
      typed.destroy();
    };
  });
  return (
    <Box>
      <Box
        w="100%"
        h={{ base: "150px", md: "200px" }}
        bgImage="url('/bgProfile.webp')"
        bgRepeat="no-repeat"
        bgSize="cover"
      ></Box>
      <Flex
        display={{ md: "none", base: "flex" }}
        pos="absolute"
        right="4"
        top="4"
      >
        <ColorMode />
      </Flex>
      <Flex justify="space-between">
        <ImgProfile
          src={imgProf}
          alt="Profile"
          w={{ base: "125px", md: "150px" }}
          borderRadius="1000px"
          border="4px solid"
          borderColor={useColorModeValue("white", "gray.900")}
          mt={{ base: -65, md: -78 }}
          ml={4}
        />
        <Flex
          color={useColorModeValue("gray.700", "gray.200")}
          w={{ base: "130px", md: "170px" }}
          mt={4}
          mr={{ base: 5, md: 0 }}
          justify="space-between"
        >
          <a target="_blank" href="mailto:rizkiwahyudie@gmail.com">
            <UilAt size="25" />
          </a>
          <a target="_blank" href="https://www.linkedin.com/in/rizki-wahyudie/">
            <UilLinkedin size="25" />
          </a>
          <a target="_blank" href="https://www.github.com/rizkiwahyudie">
            <UilGithub size="25" />
          </a>
          <a target="_blank" href="https://www.instagram.com/rizkiiwhyd">
            <UilInstagram size="25" />
          </a>
        </Flex>
      </Flex>
      <Box w={{ base: "93%", md: "100%" }} mx={{ base: "auto", md: "0" }}>
        <Heading mt={3}>Rizki Wahyudie</Heading>
        <Flex mb={1}>
          <Heading
            bgGradient="linear(to-l, teal.200, blue.500)"
            bgClip="text"
            fontSize={{ base: "xl", md: "2xl" }}
            ref={desc}
          ></Heading>
          <Heading>&nbsp;</Heading>
        </Flex>
        <DescItem />
        <Box h={20}></Box>
        <TitleSection
          title="Featured Portfolio"
          subTitle="Check out my featured portfolio, feel free to explore it."
        />
        <Grid templateColumns={{ sm: "repeat(4, 1fr)" }} gap={7}>
          {NEWEST_PORTFOLIO.map((item) => (
            <GridItem key={item.title} colSpan={2}>
              <HomeCard
                title={item.title}
                desc={item.desc}
                cover={item.cover}
                tech={item.tech}
                link={item.link}
              />
            </GridItem>
          ))}
        </Grid>
      </Box>
    </Box>
  );
}

const DescItem = () => {
  return (
    <div>
      <Text pb={3}>
        Hello👋, I&#39;m Muhammad Rizki Wahyudie, a guy who loves to Code,
        Alghoritma and Madrid. Welcome to my personal website, I like to share
        my <b>various thoughts</b> about web development related topics, general
        daily life and a place for&nbsp;
        <b>showcasing my portfolio</b>.
      </Text>
      <Text pb={3}>
        As a <b>developer</b>, I started <b>web development</b> when I started
        college and gradually improved my skills over time.
      </Text>
      <Text pb={3}>
        I am passionate about <b>Frontend Development</b> and enjoy working on
        the Web. I love combining my technical knowledge and creativity to build
        engaging and user-friendly websites and applications.
      </Text>
      <Text pb={3}>
        I&#39;m very interested with <b>Frontend Architecture</b>,{" "}
        <b>Frontend Accessibility</b>, and <b>User Experience</b>.
      </Text>
      <Text pb={3} display={{ base: "none", md: "block" }}>
        <b>As a person</b>, <b>I am constantly striving to improve myself</b>{" "}
        and <b>become a better person</b>. I believe that growth and personal
        development are important aspects of a <b>fulfilling life</b>.
      </Text>
    </div>
  );
};
