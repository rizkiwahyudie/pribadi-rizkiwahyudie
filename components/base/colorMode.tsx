import {
  Stack,
  useColorMode,
  IconButton,
  useColorModeValue,
} from "@chakra-ui/react";
import { MoonIcon, SunIcon } from "@chakra-ui/icons";

export default function ColorMode() {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Stack direction={"row"} spacing={6} alignItems="center">
      <IconButton
        bg={{
          base: useColorModeValue("white", "gray.200"),
          md: useColorModeValue("gray.100", "gray.700"),
        }}
        color={{
          base: useColorModeValue("gray.800", "gray.800"),
          md: useColorModeValue("gray.800", "gray.200"),
        }}
        _hover={{
          bg: {
            base: useColorModeValue("gray.100", "gray.300"),
            md: useColorModeValue("gray.200", "gray.600"),
          },
        }}
        borderRadius={{ base: "1000px", md: "12px" }}
        aria-label="Toggle Mode"
        onClick={toggleColorMode}
      >
        {colorMode === "light" ? <SunIcon /> : <MoonIcon />}
      </IconButton>
    </Stack>
  );
}
