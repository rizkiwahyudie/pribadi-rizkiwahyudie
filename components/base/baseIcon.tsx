import Image from "next/image";
import { chakra } from "@chakra-ui/react";

export const BaseIcon = ({ tech }: any) => {
  const ImgIcon = chakra(Image);
  return <ImgIcon src={tech} alt={tech} style={{ width: "20px" }} />;
};
