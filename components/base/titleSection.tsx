import { Box, Heading, Text } from "@chakra-ui/react";

interface Props {
  title: string;
  subTitle: string;
}

export default function TitleSection({ title, subTitle }: Props) {
  return (
    <Box mb={8}>
      <Heading fontSize="3xl">{title}</Heading>
      <Text>{subTitle}</Text>
    </Box>
  );
}
