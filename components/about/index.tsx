import {
  Box,
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Text,
  Heading,
  useColorModeValue,
  Flex,
  chakra,
} from "@chakra-ui/react";
import TitleSection from "../base/titleSection";
import { DATA_EXPERIENCE } from "./aboutDataExperience";
import Image from "next/image";

export default function About() {
  const subTitleExperience = useColorModeValue("gray.500", "gray.300");
  const LogoComp = chakra(Image);
  return (
    <>
      <Box
        w={{ base: "93%", md: "100%" }}
        mx={{ base: "auto", md: "0" }}
        mt={5}
      >
        <TitleSection
          title="My Experience"
          subTitle="All about my journey in Professional Job"
        />
      </Box>
      <Accordion defaultIndex={[0]} allowMultiple>
        {DATA_EXPERIENCE.map((item, index) => (
          <AccordionItem key={index}>
            <AccordionButton _hover={{ bg: "transparent" }}>
              <Box flex="1" textAlign="left">
                <Flex>
                  <LogoComp
                    src={item.logo}
                    alt={item.title}
                    w="48px"
                    h="48px"
                    mr={3}
                    rounded="full"
                  />
                  <Box>
                    <Heading fontSize={{ base: "xl", md: "22px" }}>
                      {item.title}
                    </Heading>
                    <Text fontSize="sm" fontWeight="medium">
                      {item.subtitle} · {item.state}
                    </Text>
                    <Text fontSize="sm" mb={-2.9} color={subTitleExperience}>
                      {item.date} · {item.contract}
                    </Text>
                    <Text fontSize="sm" color={subTitleExperience}>
                      {item.location} · {item.work}
                    </Text>
                  </Box>
                </Flex>
              </Box>
              <AccordionIcon display={{ base: "none", md: "block" }} />
            </AccordionButton>
            <AccordionPanel pb={8}>
              <Text>{item.desc}</Text>
              <Box
                ml={5}
                mt={2}
                fontSize={{ base: "15px", md: "md" }}
                textAlign="justify"
              >
                <ul>
                  {item.jobdesc.map((item, index) => (
                    <li key={index}>{item}</li>
                  ))}
                </ul>
              </Box>
              <Box mt={2} textAlign="justify">
                <Text
                  as="span"
                  fontWeight="semibold"
                  fontSize={{ base: "15px", md: "md" }}
                >
                  Skills :{" "}
                </Text>{" "}
                {item.skills.map((item, index) => (
                  <Text
                    as="span"
                    key={index}
                    fontWeight={{ base: "medium", md: "normal" }}
                    fontSize={{ base: "14px", md: "md" }}
                  >
                    {item} ·{" "}
                  </Text>
                ))}
              </Box>
            </AccordionPanel>
          </AccordionItem>
        ))}
      </Accordion>
    </>
  );
}
