import logoVocasia from "@/public/experience/logo-vocasia.webp";
import logo931 from "@/public/experience/logo-931.webp";
import logoAmikom from "@/public/experience/logo-amikom.webp";
import vocasia1 from "../../public/experience/exp-vocasia-1.webp";
import img931 from "../../public/experience/exp-931-1.webp";
import amikom1 from "../../public/experience/exp-amikom-1.webp";

interface Experience {
  logo: any,
  title: string,
  subtitle: string,
  state: string,
  date: string,
  contract: string,
  location: string,
  work: string,
  desc: string,
  jobdesc: Array<String>,
  skills: Array<String>,
  activity: Array<Object>,
}

export const DATA_EXPERIENCE: Array<Experience> = [
  {
    logo: logoVocasia,
    title: "Frontend Developer",
    subtitle: "Vocasia",
    state: "Internship",
    date: "Feb 2023 - Now",
    contract: "2 month",
    location: "Jakarta Timur, Indonesia",
    work: "WFO",
    desc: "Vocasia is the best certified online course site to improve skills in various fields for students, professionals and the general public.",
    jobdesc: [
      "Learning the Tech Stack used in Vocasia such as JS ES6+, Typescript, React Hooks, Nextjs, Chakra UI for CSS framework, SWR, and Zustand ",
      "Having daily meetings with the SCRUM Master Team and Engineer Team (Web Frontend, Apps Frontend, Backend, UI/UX) regarding the progress of the project worked on and explaining the obstacles encountered during the work",
      "Having discussions with the web Frontend team regarding constraints and resolving issues on bugs while coding",
      "Implementing the design of the UI / UX Team into the code, and providing other interactive such as animations and others to make the website more user friendly",
      "Debugging bugs found during project work with team and mentor",
      "Testing in Postman first THE REST API provided by the backend to make sure the data is in accordance with the needs that have been determined",
      "Consuming API related data provided from Backend",
      "Make a revision if there is an error after the results of the tests conducted QA Tester",
    ],
    skills: [
      "React.js",
      "React Hooks",
      "Typescript",
      "Nextjs",
      "Chakra UI",
      "Git",
      "Gitlab",
      "Debugging",
      "Collaborative Problem Solving",
      "Team Leadership",
      "Teamwork",
      "Time Management",
    ],
    activity: [
      {
        title: "Onboarding Vocasia",
        image: vocasia1,
      },
    ],
  },
  {
    logo: logo931,
    title: "Jr. Frontend Developer",
    subtitle: "913 Community",
    state: "Contract",
    date: "Jan 2022 - Feb 2022",
    contract: "2 month",
    location: "Yogyakarta, Indonesia",
    work: "WFH",
    desc: "Community based on RnD Project focused on creating Doge Trading Bot project",
    jobdesc: [
      "Implementation of the Design interface to the program",
      "Code collaboration with front end teams through gitlab",
    ],
    skills: ["Javascript", "Bootstrap", "jQuery", "Git", "Gitlab"],
    activity: [
      {
        title: "Project Documentation on Gitlab",
        image: img931,
      },
    ],
  },
  {
    logo: logoAmikom,
    title: "Web Developer",
    subtitle: "Universitas AMIKOM",
    state: "Internship",
    date: "Sept 2022 - Dec 2022",
    contract: "4 month",
    location: "Yogyakarta, Indonesia",
    work: "WFH",
    desc: "Online internship training Program Academy AMIKOM Center",
    jobdesc: [
      "Learning PHP Laravel Framework with Mentor",
      "Create a portfolio project of Learning Management System web applications using Laravel",
    ],
    skills: ["PHP", "Laravel", "Bootstrap", "Git", "Gitlab"],
    activity: [
      {
        title: "Sertification Completion",
        image: amikom1,
      },
    ],
  },
];
