import React, { ReactNode } from "react";
import { Box, useColorModeValue } from "@chakra-ui/react";
import Navbar from "./navbar";
import Footer from "./footer";

type Props = {
  children: ReactNode;
};

export default function Layout({ children }: Props) {
  return (
    <>
      <Navbar />
      <Box bg={useColorModeValue("white", "gray.900")}>
        <Box mx="auto" maxW={{ base: "100%", md: "90%", lg: "60%", xl: "50%" }}>
          {children}
        </Box>
      </Box>
      <Footer />
    </>
  );
}
