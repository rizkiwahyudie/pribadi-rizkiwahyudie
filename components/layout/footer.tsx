import { Flex, useColorModeValue, chakra, Box } from "@chakra-ui/react";
import Link from "next/link";
import {
  UilAt,
  UilLinkedin,
  UilGithub,
  UilInstagram,
} from "@iconscout/react-unicons";

type Props = {};

export default function Footer({}: Props) {
  const NavRoute = chakra(Link);
  return (
    <footer>
      <Box bg={useColorModeValue("white", "gray.900")}>
        <Box p="30px"></Box>
        <Box
          w={{ base: "100%", md: "90%", lg: "60%", xl: "50%" }}
          borderTop="1px solid #4A5568"
          mx="auto"
          px={{ base: 10, md: 0 }}
        >
          <Flex
            my={5}
            justify="space-between"
            flexDir={{ base: "column", md: "row" }}
            color={useColorModeValue("gray.800", "gray.400")}
          >
            <Flex flex={1} flexDir={{ base: "column", md: "row" }}>
              {NAV_ITEMS.map((item) => (
                <NavRoute
                  mr={{ base: 0, md: 5 }}
                  mb={{ base: 3, md: 0 }}
                  href={item.href}
                  key={item.label}
                >
                  {item.label}
                </NavRoute>
              ))}
            </Flex>
            <Flex
              flex={1}
              justify="flex-end"
              flexDir={{ base: "column", md: "row" }}
            >
              <NavRoute mb={{ base: 3, md: 0 }} href="/certificate">
                Certificate
              </NavRoute>
              <NavRoute ml={{ base: 0, md: 5 }} href="/resume">
                Resume
              </NavRoute>
            </Flex>
          </Flex>
          <Flex
            color={useColorModeValue("gray.700", "gray.200")}
            w="180px"
            pb={10}
            justify="space-between"
          >
            <a target="_blank" href="mailto:rizkiwahyudie@gmail.com">
              <UilAt size="30" />
            </a>
            <a
              target="_blank"
              href="https://www.linkedin.com/in/rizki-wahyudie/"
            >
              <UilLinkedin size="30" />
            </a>
            <a target="_blank" href="https://www.github.com/rizkiwahyudie">
              <UilGithub size="30" />
            </a>
            <a target="_blank" href="https://www.instagram.com/rizkiiwhyd">
              <UilInstagram size="30" />
            </a>
          </Flex>
        </Box>
      </Box>
    </footer>
  );
}

interface Nav_Items {
  label: string,
  href?: any
}

const NAV_ITEMS: Array<Nav_Items> = [
  {
    label: "Home",
    href: "/",
  },
  {
    label: "Blog",
    href: "/blog",
  },
  {
    label: "Portfolio",
    href: "/portfolio",
  },
  {
    label: "Guest Book",
    href: "/guest-book",
  },
];
