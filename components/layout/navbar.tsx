import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import {
  Box,
  Flex,
  Text,
  Stack,
  Popover,
  PopoverTrigger,
  useColorModeValue,
  chakra,
} from "@chakra-ui/react";
import Link from "next/link";
import {
  UilEstate,
  UilNotebooks,
  UilClipboardAlt,
  UilRocket,
} from "@iconscout/react-unicons";
import ColorMode from "../base/colorMode";

export default function Navbar() {
  const [isScrolled, setIsScrolled] = useState(false);
  const [isMobile, setMobile] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });

  const handleScroll = () => {
    if (window.pageYOffset > 0) {
      setIsScrolled(true);
    } else {
      setIsScrolled(false);
    }
    if (window.pageYOffset > 30) {
      setMobile(true);
    } else {
      setMobile(false);
    }
  };

  return (
    <Box>
      <Box
        position="fixed"
        left="50%"
        transform="translateX(-50%)"
        top={{ md: 0 }}
        bottom={{ base: isMobile ? -20 : 0 }}
        w="100%"
        bg={{
          base: useColorModeValue("gray.50", "gray.800"),
          md: useColorModeValue(
            isScrolled ? "rgb(250,250,250, 0.75)" : "white",
            "rgb(23, 25, 35, 0.8)"
          ),
        }}
        backdropFilter={{ base: "0", md: "blur(20px)" }}
        color={useColorModeValue("gray.800", "white")}
        boxShadow={{ base: "md", md: "none" }}
        h={{ md: "60px" }}
        py={{ md: 2 }}
        borderBottom={{ base: "0", md: isScrolled ? "1px solid #4A5568" : "0" }}
        transition="0.3s"
      >
        <Flex
          alignItems={"center"}
          maxW={{ base: "100%", md: "90%", lg: "60%", xl: "50%" }}
          mx="auto"
        >
          {/* DESKTOP */}
          <DesktopNav />
          <Flex display={{ base: "none", md: "flex" }} justify={"flex-end"}>
            <ColorMode />
          </Flex>
          {/* MOBILE */}
          <MobileNav />
        </Flex>
      </Box>
      <Box p={{ base: "0", md: "30px" }}></Box>
    </Box>
  );
}

const DesktopNav = () => {
  const linkColor = useColorModeValue("gray.800", "gray.200");
  const linkHoverColor = useColorModeValue("gray.800", "white");
  const router = useRouter();
  const currentRoute = router.pathname;
  const NavRoute = chakra(Link);

  return (
    <Flex
      flex={{ base: 1 }}
      justify="start"
      display={{ base: "none", md: "flex" }}
    >
      <Stack direction={"row"} spacing={4}>
        {NAV_ITEMS.map((navItem) => (
          <Box key={navItem.label}>
            <Popover trigger={"hover"} placement={"bottom-start"}>
              <PopoverTrigger>
                <NavRoute
                  my={2}
                  mr={4}
                  pb={0.8}
                  href={navItem.href}
                  fontSize={"lg"}
                  fontWeight={500}
                  color={linkColor}
                  borderBottom={
                    currentRoute === navItem.href ? "2px dashed" : "none"
                  }
                  _hover={{
                    color: linkHoverColor,
                  }}
                >
                  {navItem.label}
                </NavRoute>
              </PopoverTrigger>
            </Popover>
          </Box>
        ))}
      </Stack>
    </Flex>
  );
};

const MobileNav = () => {
  const router = useRouter();
  const currentRoute = router.pathname;
  const NavRoute = chakra(Link);
  return (
    <Flex
      flexDir="row"
      w="100%"
      alignItems="center"
      display={{ base: "flex", md: "none" }}
    >
      <NavRoute
        href="/"
        py={2}
        display="flex"
        flex={1}
        flexDir="column"
        position="relative"
        borderTop={currentRoute === "/" ? "2px" : "2px solid transparent"}
        borderRadius={2}
      >
        <UilEstate
          size="22px"
          style={{ marginLeft: "auto", marginRight: "auto" }}
          color={useColorModeValue("gray.800", "white")}
        />
        <Text mt={0.5} textAlign="center" fontSize="12px">
          Home
        </Text>
      </NavRoute>
      <NavRoute
        href="/about"
        py={2}
        display="flex"
        flex={1}
        flexDir="column"
        position="relative"
        borderTop={currentRoute === "/about" ? "2px" : "2px solid transparent"}
        borderRadius={2}
      >
        <UilNotebooks
          size="22px"
          style={{ marginLeft: "auto", marginRight: "auto" }}
          color={useColorModeValue("gray.800", "white")}
        />
        <Text mt={0.5} textAlign="center" fontSize="12px">
          About
        </Text>
      </NavRoute>
      <NavRoute
        href="/portfolio"
        py={2}
        display="flex"
        flex={1}
        flexDir="column"
        position="relative"
        borderTop={
          currentRoute === "/portfolio" ? "2px" : "2px solid transparent"
        }
        borderRadius={2}
      >
        <UilRocket
          size="22px"
          style={{ marginLeft: "auto", marginRight: "auto" }}
          color={useColorModeValue("gray.800", "white")}
        />
        <Text mt={0.5} textAlign="center" fontSize="12px">
          Portfolio
        </Text>
      </NavRoute>
      <NavRoute
        href="/guest-book"
        py={2}
        display="flex"
        flex={1}
        flexDir="column"
        position="relative"
        borderTop={
          currentRoute === "/guest-book" ? "2px" : "2px solid transparent"
        }
        borderRadius={2}
      >
        <UilClipboardAlt
          size="22px"
          style={{ marginLeft: "auto", marginRight: "auto" }}
          color={useColorModeValue("gray.800", "white")}
        />
        <Text mt={0.5} textAlign="center" fontSize="12px">
          Guest Book
        </Text>
      </NavRoute>
    </Flex>
  );
};

interface NavItem {
  label: string;
  href?: any;
}

const NAV_ITEMS: Array<NavItem> = [
  {
    label: "Home",
    href: "/",
  },
  {
    label: "About",
    href: "/about",
  },
  {
    label: "Portfolio",
    href: "/portfolio",
  },
  {
    label: "Guest Book",
    href: "/guest-book",
  },
];
