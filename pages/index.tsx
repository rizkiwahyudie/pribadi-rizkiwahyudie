import Head from "next/head";
import Homes from "@/components/home";

export default function Home() {
  return (
    <>
      <Head>
        <title>Rizki Wahyudie</title>
        <meta
          name="description"
          content="This is my Portfolio, Here We Go!"
        />
        <link rel="icon" href="/Rizki Logo.webp" />
      </Head>
      <Homes />
    </>
  );
}
// export async function getStaticProps() {
//   const response = await fetch(
//     "https://64143ceb1821112fbfd285a0.mockapi.io/profile"
//   );
//   const profile = await response.json();

//   return {
//     props: {
//       profile,
//     },
//   };
// }