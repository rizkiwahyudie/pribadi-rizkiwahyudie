import About from "../components/about";
import Head from "next/head";

export default function Blog() {
  return (
    <>
      <Head>
        <title>About - Rizki Wahyudie</title>
        <meta name="description" content="This is my Portfolio, Here We Go!" />
        <link rel="icon" href="/Rizki Logo.webp" />
      </Head>
      <About />
    </>
  );
}
